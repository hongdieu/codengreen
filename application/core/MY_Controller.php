<?php
class MY_Controller extends CI_Controller
{
    public $data = array();
    
    function __construct()
    {
        parent::__construct();
        $check = $this->uri->segment(1);
        switch ($check) {
            case 'admin':
            {
                //$this->load->helper('admin');
                $this->_check_login();
                # code...
                break;
            }
                
            
            default:
                # code...
                break;
        }
    }
    private function _check_login()
    {
        $controller = $this->uri->segment(2);
       // echo $controller;
        $login = $this->session->userdata('login');
        if(!$login && $controller != 'login')
        {
            
            redirect(base_url('admin/login'),'refresh');
            
        }
        if($login && $controller == 'login')
        {
            
            redirect(base_url('admin/home'),'refresh');
            
        }
    }

}
?>
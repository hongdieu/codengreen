

		<link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/>
  <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/>

        <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
  <script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
  <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<div class="card">
    <div class ="row">

        <div class="header">
            <ol class="breadcrumb" id = "menubreadcrumb">
                <li id ="first">
                    <a href="#" class="glyphicon glyphicon-home"></a>
                </li>
                <li class="active"> <a> Quản lý vườn </a></li>
            
                <li><a href="<?php echo base_url('admin/garden/create');?>">Thêm mới</a></li>
            </ol>
    
        </div>
    </div>
    <div class ="row">
    <div class ="col-md-2"></div>
    <div class ="col-md-8" style="border: 1px #eee solid; margin-bottom:50px;border-radius: 8px;
box-shadow: 1px 1px 1px 1px #FFF;">

    
<div class ="body" >
<h3 style= "color:rgba(44, 102, 10, 0.77);" ><strong>
<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
Thay đổi thông tin  
            <strong>
        </h3>
<hr/ style="border-top: 1px solid rgba(50, 122, 8, 0.77);">
        <form action="" method="POST" class="form-horizontal" role="form" style= "color:#000">
 <?php
 if(isset($getGardenId))
 
 foreach ($getGardenId->data as $r) {
     # code...
 
 ?>     
        <div class = "col-md-8"> 
             <h5> Tên vườn rau  </h5>                              
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập tên vườn rau" name ="name" type="text" value = "<?php echo $r->name;?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('name'); ?> </i> </small></h4>            
            
        </div>

        <div class = "col-md-4"> 
             <h5> Tên người dùng  </h5>                                                           
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <select class="form-control show-tick" data-live-search="true" name = "userName">
                    <?php foreach ($getUser->data as $us) {
                        # code...
                        if($r->userId == $us->_id)
                        echo "<option value = '".$us->_id."' selected>".$us->username."</option>";
                        else 
                        echo "<option value = '".$us->_id."'>".$us->username."</option>";
                    }?>
                        
                        
                    </select>
                </div>
            </div>
             
                
        </div>

        <div class = "col-md-8">
        <h5>Địa chỉ:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="glyphicon glyphicon-home"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ..." value = "<?php if(isset($r->address)) echo $r->address;?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('address'); ?> </i> </small></h4>            
            
        </div>

        <div class = "col-md-4">
        <h5>Ngày tạo:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="startDate" id="date">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('startDate'); ?> </i> </small></h4>            
        </div>

        <div class="col-md-3">
        <h5>Trạng thái </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <select class="form-control show-tick" name = "isActive">
                        <?php if($r->isActive) 
                        echo "<option value = 'true'selected>Hoạt động</option>
                        <option value = 'false'>Không</option>";
                        else "<option value = 'true'>Hoạt động</option>
                        <option value = 'false' selected>Không</option>";  ?>
                       
                    </select>
                </div>
            </div>
           

        </div>
        <div class="col-md-9"></div>
        <div class="col-md-12">
        <button type="submit" class="btn btn-success waves-effect pull-right" name="edit" style="margin-bottom:50px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sửa</button>   
        </div>    
                <?php }?>

        </form>
    </div>

    <?php
        if(isset($getZone))
        {
            echo "<div class='col-md-12'><h4>Danh sách khu</h4></div>";
            foreach ($getZone->data as $key => $zone) {
            # code...
    ?>
    <div class="col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $zone->name;?></h3>
            </div>
            <div class="panel-body">
                    <h5> Vị trí:  <?php echo $zone->location;?> </h5>
                    <h5> Kiểu:  <?php  echo $zone->type;?> </h5>
                    <h5> Nhiệt độ:  <?php  echo $zone->nhietdo;?> </h5>
            </div>
        </div>
    </div>
    
        <?php }}?>

    </div>
    <div class="col-md-2"></div>

    
</div>


<script>
  $(document).ready(function()
  {
      $('#date').bootstrapMaterialDatePicker
      ({
          time: false,
          clearButton: true
      });

  });  
</script>





		<link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/>
  <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/>

        <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
  <script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
  <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<div class="card">
    <div class ="row">

        <div class="header">
            <ol class="breadcrumb" id = "menubreadcrumb">
                <li id ="first">
                    <a href="#" class="glyphicon glyphicon-home"></a>
                </li>
                <li class="active"> <a> Quản lý vườn </a></li>
            
                <li><a href="<?php echo base_url('admin/garden');?>">Danh sách</a></li>
            </ol>
    
        </div>
    </div>
    <div class ="row">
    <div class ="col-md-2"></div>
    <div class ="col-md-8" style="border: 1px #eee solid; margin-bottom:50px;border-radius: 8px;
box-shadow: 1px 1px 1px 1px #FFF;">

    
<div class ="body" >
<h3 style= "color:rgba(44, 102, 10, 0.77);" ><strong>
<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
Thêm vườn rau 
            <strong>
        </h3>
<hr/ style="border-top: 1px solid rgba(50, 122, 8, 0.77);">
        <form action="" method="POST" class="form-horizontal" role="form" style= "color:#000">
      
        <div class = "col-md-8"> 
             <h5> Tên vườn rau  </h5>                              
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập tên vườn rau" name ="name" type="text">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('name'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-4"> 
             <h5> Tên người dùng  </h5>                                                           
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <select class="form-control show-tick" data-live-search="true" name = "fullname">
                    <?php foreach ($getUser->data as $us) {
                        # code...
                        echo "<option value = '".$us->_id."'>".$us->username."</option>";
                    }?>
                    </select>
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('fullname'); ?> </i> </small></h4>

                
        </div>

        <div class = "col-md-8">
        <h5>Địa chỉ:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="glyphicon glyphicon-home"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ...">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('address'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-4">
        <h5>Ngày tạo:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" id = "date" class="form-control" name="date">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('date'); ?> </i> </small></h4>            
        </div>

        <div class="col-md-3">
        <h5>Trạng thái </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <select class="form-control show-tick" name = "isActive">
                        <option value = "true">Hoạt động</option>
                        <option value = "false">Không</option>
                    </select>
                </div>
            </div>
           

        </div>
        <div class="col-md-9"></div>
        
        <button type="submit" class="btn btn-success waves-effect pull-right" name="edit" style="margin-bottom:50px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Thêm</button>   
            


        </form>
    </div>

    </div>
    <div class="col-md-2"></div>
</div>

<script>
  $(document).ready(function()
  {
      $('#date').bootstrapMaterialDatePicker
      ({
          time: false,
          clearButton: true
      });

  });  
</script>


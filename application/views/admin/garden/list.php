

<script>
    function cl_delete(){
        if(confirm("Bạn có chắc chắn muốn xóa?")==false)
        {
			$("#delete").attr('href', '#');
        }

    }
</script>
<div class="card">
<div class = "row">
    <div class="header">
        
        <ol class="breadcrumb" id = "menubreadcrumb">
            <li id ="first">
                <a href="#" class="glyphicon glyphicon-home"></a>
            </li>
            <li class="active"> <a> Quản lý vườn </a></li>
          
            <li><a href="<?php echo base_url('admin/garden/create');?>">Thêm mới</a></li>
        </ol>
        
     
    </div>
</div>

    <div class="body">
  
        <h3 style="text-align:center;"><strong>
            Danh sách vườn  
            <strong>
        </h3>
<hr/>

<?php
if(isset($message_success))
{
    echo "<div class ='notification-success'> <h5 >".$message_success."</h5></div>";
}
if(isset($message_error))
{
    echo "<div class ='notification-error'> <h5 >".$message_error."</h5></div>";
}

?>
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped " id ="list">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên vườn rau</th>
                        <th>Tên người dùng</th>
                        <th>Ngày tạo</th>                        
                        <th>Địa chỉ</th>
                        <th>Trạng thái</th>
                        <th>Chọn </th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($list->data as $r) {
                    # code...
                ?>
                        <td><?php echo $i?> </td>
                        <td><?php echo $r->name;?> </td>
                        <td>
                        <?php 
                        foreach ($getUser->data as $us) {
                            # code...
                            if($us->_id == $r->userId)
                            {
                                echo $us->username;
                                break;
                                
                            }
                        }
                        ?>
                        </td>

                        <td><?php echo $r->startDate;?></td>
                        <td><?php  if(isset($r->address)) echo $r->address;?></td>  

                        <td><?php if($r->isActive) echo "Hoạt động"; else echo "Không";?> </td>
                        <td>
<a href="garden/edit/<?php echo $r->_id;?>" class="glyphicon glyphicon-pencil" title ="Chỉnh sửa" style="margin-right: 5px;"></a>  <a href="garden/delete/<?php echo $r->_id;?>" class="glyphicon glyphicon-remove" style="color:red;" title ="Xóa" onclick = "cl_delete()" id="delete"  ></a></td>
                    </tr>
                <?php $i++; }?>
                </tbody>
            </table>
        </div>
        
    </div>
</div>



		<link rel="stylesheet"
			  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/>
		<link rel="stylesheet"
			  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/>

              <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
		<script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
		<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>

	
		
<div class="card">
    <div class ="row">

        <div class="header">
            <ol class="breadcrumb" id = "menubreadcrumb">
                <li id ="first">
                    <a href="#" class="glyphicon glyphicon-home"></a>
                </li>
                <li class="active"> <a> Quản lý admin </a></li>
            
                <li><a href="<?php echo base_url('admin/user');?>">Danh sách</a></li>
            </ol>
    
        </div>
    </div>
    <div class ="row">
    <div class ="col-md-2"></div>
    <div class ="col-md-8" style="border: 1px #eee solid; margin-bottom:50px;border-radius: 8px;
box-shadow: 1px 1px 1px 1px #FFF;">

    
<div class ="body" >
<h3 style= "color:rgba(44, 102, 10, 0.77);" ><strong>
<i class="fa fa-plus-circle" aria-hidden="true"></i>
Tạo tài khoản
            <strong>
        </h3>
<hr/ style="border-top: 1px solid rgba(50, 122, 8, 0.77);">
    <form action="" method="POST" class="form-horizontal" role="form" style= "color:#000">
    <div class = "row">
        <div class = "col-md-6"> 
             <h5> Họ và tên:  </h5>                              
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập họ và tên" name ="fullname" type="text" value = "<?php echo set_value('fullname');?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('fullname'); ?> </i> </small></h4>
        </div>
        
        <div class = "col-md-6"> 
             <h5> Tên người dùng  </h5>                              
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập tên người dùng" name ="username" type="text" value = "<?php echo set_value('username');?>">
                </div>
            </div>
        </div>
        <h4 ><small><i style="color:#F44336"> <?php echo form_error('username'); ?> </i> </small></h4>
        
    </div>

    <div class = "row">    
        <div class = "col-md-6"> 
             <h5> Mật khẩu </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">lock</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập mật khẩu" name ="password" type="password" value = "<?php echo set_value('password');?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('password'); ?> </i> </small></h4>            
        </div>
        
        <div class = "col-md-6">  
        <h5> Email:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="material-icons">email</i>
                </span>
                <div class="form-line">
                    <input type="email" class="form-control email" name ="email" placeholder="Ex: example@example.com" value = "<?php echo set_value('email');?>">
                </div>
            </div>
        </div>
        <h4 ><small><i style="color:#F44336"> <?php echo form_error('email'); ?> </i> </small></h4>
        
    </div>
    
    <div class = "row"> 
        <div class = "col-md-6"> 
             <h5> Nhập lại mật khẩu </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">lock</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập lại mật khẩu" name ="re_password" type="password" value = "<?php echo set_value('re_password');?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('re_password'); ?> </i> </small></h4>
            
        </div>
        <div class = "col-md-6">
        <h5>Số điện thoại:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="material-icons">phone</i>
                </span>
                <div class="form-line">
                    <input type="tel" class="form-control mobile-phone-number" name ="phone" placeholder="Ex: +00 (000) 000-00-00" value = "<?php echo set_value('phone');?>">
                </div>
            </div>
        </div>
        <h4 ><small><i style="color:#F44336"> <?php echo form_error('phone'); ?> </i> </small></h4>        
    </div>
    
    <div class = "row"> 
        <div class = "col-md-8">
        <h5>Địa chỉ:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="glyphicon glyphicon-home"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ..." value = "<?php echo set_value('address');?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('address'); ?> </i> </small></h4>            
        </div>
        <div class = "col-md-4">
        <h5>Ngày sinh:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id = "date" name="date">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('date'); ?> </i> </small></h4>            
        </div>
        <button type="submit" class="btn btn-success waves-effect pull-right" name="create" style="margin-bottom:50px;"><i class="fa fa-check-circle"></i>  Thêm</button>   
            
        </div>
    </div>
    </form>
    </div>

    </div>
    <div class="col-md-2"></div>
</div>

<script type="text/javascript">
		$(document).ready(function()
		{
			$('#date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});

		});
		</script>

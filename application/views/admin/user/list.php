

<script>
    function cl_delete(){
      

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                $("#delete").attr('href', '#');
                swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
    )
  }
})

    }
    

</script>


<div class="card">
<div class = "row">
    <div class="header">
        
        <ol class="breadcrumb" id = "menubreadcrumb">
            <li id ="first">
                <a href="#" class="glyphicon glyphicon-home"></a>
            </li>
            <li class="active"> <a> Quản lý admin </a></li>
          
            <li><a href="<?php echo base_url('admin/user/create');?>">Thêm mới</a></li>
        </ol>
        
     
    </div>
</div>

    <div class="body">
  
        <h3 style="text-align:center;     color: rgba(44, 102, 10, 0.77);"><strong>
            Danh sách người dùng  
            <strong>
        </h3>
<hr/>
<?php
if(isset($message_success))
{
    echo "<div class ='notification-success'> <h5 >".$message_success."</h5></div>";
}
if(isset($message_error))
{
    echo "<div class ='notification-error'> <h5 >".$message_error."</h5></div>";
}

?>
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped " id = "list">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên người dùng</th>
                        <th>Họ tên</th>
                        <th>Ngày sinh</th>                        
                        <th>Địa chỉ</th>
                        <th>Số điện thoại</th>
                        <th>Email</th>
                        <th>Chọn </th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($list->data as $r) {
                    # code...
                ?>
                    <tr>
                        <td><?php echo $i;?></td>   
                        <td><?php echo $r->username;?></td>                     
                        <td><?php echo $r->fullname;?></td>                     
                        <td><?php echo $r->birthday;?></td>                     
                        <td><?php echo $r->address;?></td>                     
                        <td><?php echo $r->phone;?></td>                     
                        <td><?php echo $r->email;?></td>                     
                       
                        <td>
<a href="user/edit/<?php echo $r->_id;?>" class="glyphicon glyphicon-pencil" title ="Chỉnh sửa" style="margin-right: 5px;"></a>  <a href="user/delete/<?php //echo $r->_id;?>" id="delete" class="glyphicon glyphicon-remove" style="color:red;" title ="Xóa" onclick = "cl_delete()" ></a></td>
                    </tr>
                <?php $i++; }?>
                </tbody>
            </table>
        </div>
        
    </div>
    
</div>



<div class="card">
    <div class ="row">

        <div class="header">
            <ol class="breadcrumb" id = "menubreadcrumb">
                <li id ="first">
                    <a href="#" class="glyphicon glyphicon-home"></a>
                </li>
                <li class="active"> <a> Quản lý admin </a></li>
            
                <li><a href="<?php echo base_url('admin/user/create');?>">Thêm mới</a></li>
            </ol>
    
        </div>
    </div>
    <div class ="row">
    <div class ="col-md-2"></div>
    <div class ="col-md-8" style="border: 1px #eee solid; margin-bottom:50px;border-radius: 8px;
box-shadow: 1px 1px 1px 1px #FFF;">
<?php
if(isset($getUserId))
foreach ($getUserId->data as $r) {
    # code...

?>
    
<div class ="body" >
<h3 style= "color:rgba(44, 102, 10, 0.77);" ><strong>
<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
Thay đổi thông tin  
            <strong>
        </h3>
        <hr/ style="border-top: 1px solid rgba(50, 122, 8, 0.77);">
        <form action="" method="POST" class="form-horizontal" role="form" style= "color:#000">
      
        <div class = "col-md-6"> 
             <h5> Họ và tên:  </h5>                              
            <div class="input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập họ tên" name ="fullname" type="text" value = "<?php echo $r->fullname;?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('fullname'); ?> </i> </small></h4>            

        </div>

        <div class = "col-md-6"> 
             <h5> Mật khẩu </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                    <i class="material-icons">lock</i>
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="" name ="password" type="password" >
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('password'); ?> </i> </small></h4>            

        </div>
        
        <div class = "col-md-6">  
        <h5> Email:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="material-icons">email</i>
                </span>
                <div class="form-line">
                    <input type="email" class="form-control email" name ="email" placeholder="Ex: example@example.com"  value = "<?php echo $r->email;?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('email'); ?> </i> </small></h4>            

        </div>

        <div class = "col-md-6">
        <h5>Số điện thoại:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="material-icons">phone</i>
                </span>
                <div class="form-line">
                    <input type="tel" class="form-control mobile-phone-number" name ="phone" placeholder="Ex: +00 (000) 000-00-00"  value = "<?php echo $r->phone;?>">
                </div>
            </div>
        </div>
        <h4 ><small><i style="color:#F44336"> <?php echo form_error('phone'); ?> </i> </small></h4>            

        <div class = "col-md-12">
        <h5>Địa chỉ:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="glyphicon glyphicon-home"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ..."  value = "<?php echo $r->address;?>">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('address'); ?> </i> </small></h4>            
        
        </div>
            <div class = "col-md-12">
            <button type="submit" class="btn btn-success waves-effect pull-right" name="edit" style="margin-bottom:50px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sửa</button>   
            </div>
          

            <h4>Danh sách vườn rau<h4>
            <hr/>
            <?php 
                if(isset($getListGarden))
            
                foreach ($getListGarden->data as $row) {
                
            ?>
        
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                                <h3 class="panel-title"><?php echo $row->name;?></h3>
                        </div>
                        <div class="panel-body" style = "height:100px">
                            <h5><?php echo $row->address;?></h5>
                            <h5><?php echo $row->startDate;?></h5>
                        </div>
                    </div>
                </div>
           
                <?php }?> 

        </div> 
        </form>
    </div>
<?php }?>
    </div>
    <div class="col-md-2"></div>
</div>

<script>
   
</script>


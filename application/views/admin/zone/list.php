
<script>
  $(function(){
    $("#listzone").dataTable();
  })
</script>
<script>
    function cl_delete(){
        if(confirm("Bạn có chắc chắn muốn xóa?")==false)
        {
			$("#delete").attr('href', '#');
        }

    }
</script>
<div class="card">
<div class = "row">
    <div class="header">
        
        <ol class="breadcrumb" id = "menubreadcrumb">
            <li id ="first">
                <a href="#" class="glyphicon glyphicon-home"></a>
            </li>
            <li class="active"> <a> Quản lý khu </a></li>
          
            <li><a href="<?php echo base_url('admin/zone/create');?>">Thêm mới</a></li>
        </ol>
        
     
    </div>
</div>

    <div class="body">
  
        <h3 style="text-align:center; color:#000;"><strong>
            DANH SÁCH KHU
            <strong>
        </h3>
        <hr/>
<?php
if(isset($message_success))
{
    echo "<div class ='notification-success'> <h5 >".$message_success."</h5></div>";
}
if(isset($message_error))
{
    echo "<div class ='notification-error'> <h5 >".$message_error."</h5></div>";
}

?>
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped " style="color:#000"  id = "list">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên khu</th>
                        <th>Tên vườn rau</th>
                        <th>Vị trí</th>
                        <th>Kiểu</th>                        
                        <th>Nhiệt độ</th>
                        <th>Độ ẩm không khí</th>
                        <th>Độ ẩm đất</th>
                        <th>Cường độ ánh sáng </th>
                        <th>Thời gian tưới </th>
                        <th>Ngày tưới gần nhất </th>
                        <th>Ngày bón phân </th>
                        <th>Chọn </th>
                        
                    </tr>
                </thead>
                <tbody style="font-weight: normal;">
                <?php
                $i = 1;
                foreach ($list->data as $r) {
                    # code...
                ?>
                        <td><?php echo $i?> </td>
                        <td><?php echo $r->name;?> </td>
                        <td>
                        <?php 
                        foreach ($getGarden->data as $gr) {
                            # code...
                            if($gr->_id == $r->gardenId)
                            {
                                echo $gr->name;
                                break;
                                
                            }
                        }
                        ?>
                        </td>

                        <td><?php echo $r->location;?></td>
                        <td><?php echo $r->type;?></td>
                        <td><?php echo $r->nhietdo;?></td>
                        <td><?php echo $r->doamkhongkhi;?></td>
                        <td><?php echo $r->doamdat;?></td>
                        <td><?php  if(isset($r->cuongdoanhsang)) echo $r->cuongdoanhsang; else echo $r->cuongdoas;?></td>
                        <td><?php echo $r->thoigiantuoi;?></td>
                        <td><?php echo $r->ngaytuoigannhat;?></td>
                        <td><?php echo $r->ngaybonphan;?></td>
                  
                        <td>
<a href="zone/edit/<?php echo $r->_id;?>" class="glyphicon glyphicon-pencil" title ="Chỉnh sửa" style="margin-right: 5px;"></a>  <a href="zone/delete/<?php echo $r->_id;?>" class="glyphicon glyphicon-remove" style="color:red;" title ="Xóa" onclick = "cl_delete()" id="delete"  ></a></td>
                    </tr>
                <?php $i++; }
                ?>
                </tbody>
            </table>
        </div>
        
    </div>
</div>




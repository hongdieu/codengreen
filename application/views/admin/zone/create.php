

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/>

<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
  <script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
  <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<div class="card">
    <div class ="row">

        <div class="header">
            <ol class="breadcrumb" id = "menubreadcrumb">
                <li id ="first">
                    <a href="#" class="glyphicon glyphicon-home"></a>
                </li>
                <li class="active"> <a> Quản lý khu </a></li>
            
                <li><a href="<?php echo base_url('admin/zone');?>">Danh sách</a></li>
            </ol>
    
        </div>
    </div>
    <div class ="row">
    <div class ="col-md-2"></div>
    <div class ="col-md-8" style="border: 1px #eee solid; margin-bottom:50px;border-radius: 8px;
box-shadow: 1px 1px 1px 1px #FFF;">

    
<div class ="body" >
<h3 style= "color:rgba(44, 102, 10, 0.77);" ><strong>
<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
Thêm vườn rau 
            <strong>
        </h3>
<hr/ style="border-top: 1px solid rgba(50, 122, 8, 0.77);">
        <form action="" method="POST" class="form-horizontal" role="form" style= "color:#000">
      
        <div class = "col-md-8"> 
             <h5> Tên khu  </h5>                              
            <div class="input-group">   
                <span class="input-group-addon">
                   
                </span>
                <div class="form-line">
                    <input class="form-control" placeholder="Nhập tên khu" name ="name" type="text" style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('name'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-4"> 
             <h5> Tên vườn rau  </h5>                                                           
            <div class="input-group">   
                <span class="input-group-addon">
                </span>
                <div class="form-line">
                    <select class="form-control show-tick" data-live-search="true" name = "gardenname">
                    <?php foreach ($getGarden->data as $gr) {
                        # code...
                        echo "<option value = '".$gr->_id."'>".$gr->name."</option>";
                    }?>
                    </select>
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('gardenname'); ?> </i> </small></h4>

                
        </div>

        <div class = "col-md-6">
        <h5>Vị trí:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="location" placeholder="Nhập vị trí..." style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('location'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-6">
        <h5>Kiểu:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-circle" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="type" placeholder="Nhập kiểu..." style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('type'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-3">
        <h5>Nhiệt độ:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-cog" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="number" class="form-control" name="nhietdo" placeholder="..." style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('nhietdo'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-3">
        <h5>Độ ẩm không khí:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-cogs" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="number" class="form-control" name="doamkhongkhi" placeholder="..." min = "0" style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('doamkhongkhi'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-3">
        <h5>Độ ẩm đất:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-empire" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="number" class="form-control" name="doamdat" placeholder="..." min = "0" style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('doamdat'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-3">
        <h5>Cường độ ánh sáng:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-spinner" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="number" class="form-control" name="cuongdoanhsang" placeholder="..." min = "0" style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('cuongdoanhsang'); ?> </i> </small></h4>

        </div>

        <div class = "col-md-4">
        <h5>Thời gian tưới:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="number" class="form-control" name="thoigiantuoi" min = "0" style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('thoigiantuoi'); ?> </i> </small></h4>            
        </div>

        <div class = "col-md-4">
        <h5>Ngày tưới gần nhất:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" id = "date1" class="form-control" name="thoigiantuoigannhat" style ="z-index:1">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('thoigiantuoigannhat'); ?> </i> </small></h4>            
        </div>

        <div class = "col-md-4">
        <h5>Ngày bón phân:  </h5>                              
            <div class=" input-group">   
                <span class="input-group-addon">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                <div class="form-line">
                    <input type="text" id = "date2" class="form-control" name="ngaybonphan">
                </div>
            </div>
            <h4 ><small><i style="color:#F44336"> <?php echo form_error('ngaybonphan'); ?> </i> </small></h4>            
        </div>
           

        </div>
        <div class="col-md-9"></div>
        
        <button type="submit" class="btn btn-success waves-effect pull-right" name="createzone" style="margin-bottom:50px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Thêm</button>   
            


        </form>
    </div>

    </div>
    <div class="col-md-2"></div>
</div>


<script>
  $(document).ready(function()
  {
      $('#date1').bootstrapMaterialDatePicker
      ({
          time: false,
          clearButton: true
      });
      $('#date2').bootstrapMaterialDatePicker
      ({
          time: false,
          clearButton: true
      });

  });  
</script>



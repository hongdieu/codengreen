<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('admin/head.php');?>    
</head>
<body class="theme-blue">
<?php $this->load->view('admin/header.php');?>    
<?php $this->load->view('admin/menuleft.php');?>    

<section class="content">
        <div class="container-fluid">
        <?php $this->load->view($temp);?>    
</div>
</section>
    
    

    </div>
</nav>


<?php $this->load->view('admin/script');?>    

</body>
</html>
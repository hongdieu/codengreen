<?php
class Logout extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
    function index()
    {
        if($this->session->userdata('login'))
        {
            $this->session->unset_userdata('login');
        }
        
        redirect('admin/login','refresh');
        
    }
}
            
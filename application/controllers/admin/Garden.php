<?php
class Garden extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('garden_model');
        $this->load->model('user_model');
    }
    function index()
    {
        
        $list = $this->garden_model->getAllGarden();
  
        $getUser = $this->user_model->getAllUser();
        $data['getUser'] = json_decode($getUser);
        $data['list'] = json_decode($list);
        $data['temp'] ='admin/garden/list';
        $this->load->view('admin/main',$data);
    }
    function edit()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $id = $this->uri->segment(4);
        $getGardenId = json_decode($this->garden_model->getGardenById($id));
        if(isset($getGardenId->exitcode))
        {
            if($getGardenId->exitcode)
            {
                $getUser = $this->user_model->getAllUser();
                
                if($this->input->post())
                {
                    $this->form_validation->set_rules('name', 'Tên vườn rau', 'required|min_length[8]');
                    $this->form_validation->set_rules('userName', 'Tên người dùng', 'required');
                    $this->form_validation->set_rules('address', 'Địa chỉ', 'required|min_length[6]');
                    $this->form_validation->set_rules('startDate', 'Ngày', 'required');
                    
                    if($this->form_validation->run())
                    {
                        $name = $this->input->post('name');
                        $idUser = $this->input->post('userName');
                        $address = $this->input->post('address');
                        $date = $this->input->post('startDate');
                        $isActive = $this->input->post('isActive');
                        $data = array(
                            "gardenId"=>$id,
                            "name"   => $name, 
                            "userId"   => $idUser,
                            "startDate"   => $date,
                            "address"    => $address,
                            "isActive"   => $isActive,
                        );
                    //   echo $name.$id.$date.$idUser.$isActive;
                       $rs = json_decode($this->garden_model->updateGarden($data));
                        $data['temp'] ='admin/garden/list';
                    }
                }
                $getZone = json_decode($this->garden_model->getZoneOfGarden($id));
                $data['getZone'] = $getZone;
                $data['getGardenId'] =$getGardenId;
                $data['getUser'] = json_decode($getUser);
                $data['temp'] ='admin/garden/edit';
                $this->load->view('admin/main',$data);
            }
        }
    }


   
    function create()
    {
        
        
        $this->load->library('form_validation');
        $this->load->helper('form');
        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Tên vườn rau', 'required|min_length[8]');
            $this->form_validation->set_rules('fullname', 'Tên người dùng', 'required');
            $this->form_validation->set_rules('address', 'Địa chỉ', 'required|min_length[6]');
            $this->form_validation->set_rules('date', 'Ngày', 'required');
            
            if($this->form_validation->run())
            {
                $name = $this->input->post('name');
                $id = $this->input->post('fullname');
                $address = $this->input->post('address');
                $date = $this->input->post('date');
                $isActive = $this->input->post('isActive');
                $data = array(
                    "name"   => $name, 
                    "userId"   => $id,
                    "startDate"   => $date,
                    "address"    => $address,
                    "isActive"   => $isActive,
                );
                //$rs = $this->user_model->createUser($data);
               // echo $name.$id.$date.$address.$isActive;
                $rs = json_decode($this->garden_model->createGarden($data));
                //echo $name.$user.$pass.$email.$phone.$address.$date.date('d/m/Y');
            }
        }
        
        $getUser = $this->user_model->getAllUser();
        $data['getUser'] = json_decode($getUser);
        $data['temp'] ='admin/garden/create';
        $this->load->view('admin/main',$data);
    }

    function delete()
    {
        if($this->uri->rsegment('2') == "delete")
        {
            $id = $this->uri->rsegment('3'); 
            $data = array(
                "gardenId"   => $id);
            $this->garden_model->deleteGarden($data);
        }
        redirect('admin/user','refresh');
    }


}

?>
<?php
class Login extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
    function index()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->view('admin/login/index');
        if($this->input->post())
        {
            $this->form_validation->set_rules('username', 'Tên người dùng','required|callback_check_login');
            $this->form_validation->set_rules('password', 'Mật khẩu','required');
            if($this->form_validation->run())
            {
                
                redirect('admin','refresh');                
            }            
        }
    }
    function check_login()
    {
       
        $user = $this->input->post('username');
        $pass = md5($this->input->post('password'));
        $kq = $this->user_model->postLogin($user,$pass);
       $k = json_decode($kq);
        if($k->exitcode)
        {
            if($user == $k->data->username && $pass == $k->data->password)
            {
                $data = array(
                    
                    "username" => $user,
                    "login"=>true,
                    "token"=> $k->token,
                    "idLogin" =>$k->data->_id,
                    "fullname"=>$k->data->fullname,
                    "email"=>$k->data->email,
                );
                $this->session->set_userdata($data);
                return true;                
            }
            $this->form_validation->set_message(__FUNCTION__,'Đăng nhập không thành công');        
            return false;
        }
        $this->form_validation->set_message(__FUNCTION__,'Đăng nhập không thành công');
        return false;
    }
}
            
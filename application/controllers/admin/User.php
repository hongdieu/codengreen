<?php
class User extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
    function index()
    {
        $message_success = $this->session->flashdata('message_success');
        $message_error = $this->session->flashdata('message_error');
        
        $list = $this->user_model->getAllUser();
        $data['message_success'] = $message_success;
        $data['message_error'] = $message_error;
        $data['list'] = json_decode($list);
        $data['temp'] ='admin/user/list';
        $this->load->view('admin/main',$data);
    }
    function edit()
    {
          
        $this->load->library('form_validation');
        $this->load->helper('form');
        $id = $this->uri->segment(4);
        $getUserId = json_decode($this->user_model->getUserByUserID($id));
        if(isset($getUserId->exitcode))
        {
            if($getUserId->exitcode)
            {
                if($this->input->post())
                {
                    $this->form_validation->set_rules('fullname', 'Họ và tên', 'required|min_length[8]');
                    $this->form_validation->set_rules('email', 'Email', 'required|min_length[6]');
                    $this->form_validation->set_rules('phone', 'Số điện thoại', 'required|min_length[6]');
                    $this->form_validation->set_rules('address', 'Địa chỉ', 'required|min_length[6]');
                
                    if($this->form_validation->run())
                    {
                        $name = $this->input->post('fullname');
                        $email = $this->input->post('email');
                        $phone = $this->input->post('phone');
                        $address = $this->input->post('address');
                        $pass = $this->input->post('password');
                        if(!isset($pass))
                            $pass="";
                        $data = array(
                           
                            "password"   => md5($pass),
                            "fullname"   => $name,
                            "address"    => $address,
                            "phone"      => $phone,
                            "email"      => $email
                        );
                        //$rs = $this->user_model->createUser($data);
                        
                        $rs = json_decode($this->user_model->updateUser($data));
                        if(isset($rs->exitcode))
                        {
                            $this->session->set_flashdata('message_success','Cập nhật thành công!');
                        } else {
                            $this->session->set_flashdata('message_error','Cập nhật không thành công!');
                        }
                        redirect('admin/user','refresh');
                        //echo $name.$user.$pass.$email.$phone.$address.$date.date('d/m/Y');
                    }
                }

                if($id == $this->session->userdata("idLogin"))
                {
                    $getListGarden = json_decode($this->user_model->getGardenOfUser($id));
                    $data['getListGarden'] = $getListGarden;
                }
                $data['getUserId'] = $getUserId;
                $data['temp'] ='admin/user/edit';
                $this->load->view('admin/main',$data);
            }
            
        }
        else {
            $this->session->set_flashdata('message_error','Tài khoản không tồn tại!');
           
            
            redirect('admin/user');
            
        }
    }

    function check_username()
    {
        $user = $this->input->post('username');
        $kt = json_decode($this->user_model->getAllUser());
        foreach ($kt->data as $r) 
            if($r->username == $user)
            {
                $this->form_validation->set_message(__FUNCTION__,'Tên người dùng đã tồn tại, vui lòng nhập lại!');
                return false;
                
            }
        return true;
    }
    function create()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        if($this->input->post())
        {
            $this->form_validation->set_rules('username', 'Ten nguoi dung', 'required|min_length[8]|callback_check_username');
            $this->form_validation->set_rules('fullname', 'Ho va ten', 'required|min_length[8]');
            $this->form_validation->set_rules('password', 'Mat khau', 'required|min_length[6]');
            $this->form_validation->set_rules('re_password', 'Nhap lai mat khau', 'required|min_length[6]');
            
            if($this->form_validation->run())
            {
                $name = $this->input->post('fullname');
                $user = $this->input->post('username');
                $pass = $this->input->post('password');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $address = $this->input->post('address');
                $date = $this->input->post('date');
                $data = array(
                    "username"   => $user, 
                    "password"   => md5($pass),
                    "fullname"   => $name,
                    "address"    => $address,
                    "phone"      => $phone,
                    "email"      => $email,
                    "birthday"   => $date,
                    "deployDate" => date('Y-m-d'),
                );
                //$rs = $this->user_model->createUser($data);
                
                $rs = json_decode($this->user_model->createUser($data));
                
                    $this->session->set_flashdata('message_success','Tạo tài khoản thành công!');
             
                redirect('admin/user','refresh');
                //echo $name.$user.$pass.$email.$phone.$address.$date.date('d/m/Y');
            }
        }


        $data['temp'] ='admin/user/create';
        $this->load->view('admin/main',$data);
    }

    function delete()
    {
        if($this->uri->rsegment('2')=='delete')
        {
            $id = $this->uri->rsegment('3');
            if($id != $this->session->userdata("idLogin"))
            $this->user_model->deleteUser($id);
        }

        
        redirect('admin/user','refresh');
        
        /*if($rs->exitcode)
            return true;
        return false;
        */

    }
    function testlist()
    {
        $list = $this->user_model->getAllUser();
      
        $data['list'] = json_decode($list);
        echo $list;
        $data['temp'] ='admin/user/testlist';
        $this->load->view('admin/main',$data);
    }


}

?>
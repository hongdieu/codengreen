<?php
class Zone extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('garden_model');
        $this->load->model('zone_model');
    }
    function index()
    {
        $message_success = $this->session->flashdata('message_success');
        $message_error = $this->session->flashdata('message_error');
        $list = $this->zone_model->getAllZone();
  
        $getGarden = $this->garden_model->getAllGarden();
        $data['getGarden'] = json_decode($getGarden);
        $data['list'] = json_decode($list);
        $data['temp'] ='admin/zone/list';
        $this->load->view('admin/main',$data);
    }


    function create()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        
        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Tên khu', 'required|min_length[5]');
            $this->form_validation->set_rules('gardenname', 'Tên vườn rau', 'required');
            $this->form_validation->set_rules('location', 'Vị trí', 'required|min_length[5]');
            $this->form_validation->set_rules('nhietdo', 'Nhiệt độ', 'required');
            $this->form_validation->set_rules('type', 'Kiểu', 'required');
            $this->form_validation->set_rules('doamkhongkhi', 'Độ ẩm không khí', 'required');
            $this->form_validation->set_rules('doamdat', 'Độ ẩm đất', 'required');
            $this->form_validation->set_rules('thoigiantuoi', 'Thời gian tưới', 'required');
            $this->form_validation->set_rules('cuongdoanhsang', 'Cường độ ánh sáng', 'required');
            $this->form_validation->set_rules('thoigiantuoigannhat', 'Thời gian tưới gần nhất', 'required');
            $this->form_validation->set_rules('ngaybonphan', 'Ngày bón phân', 'required');
            
            if($this->form_validation->run())
            {
                $name = $this->input->post('name');
                $idGr = $this->input->post('gardenname');
                $location = $this->input->post('location');
                $type = $this->input->post('type');
                $nhietdo = $this->input->post('nhietdo');
                $doamkhongkhi = $this->input->post('doamkhongkhi');
                $doamdat = $this->input->post('doamdat');
                $thoigiantuoi= $this->input->post('thoigiantuoi');
                $cuongdoanhsang = $this->input->post('cuongdoanhsang');
                $thoigiantuoigannhat = $this->input->post('thoigiantuoigannhat');
                $ngaybonphan = $this->input->post('ngaybonphan');
                $data = array(
                    "gardenId"   => $idGr,
                    "name"   => $name, 
                    "location"   => $location,
                    "type"    => $type,
                    "nhietdo"   => $nhietdo,
                    "doamkhongkhi"   => $doamkhongkhi,
                    "doamdat" => $doamdat,
                    "thoigiantuoi" => $thoigiantuoi,
                    "cuongdoas" => $cuongdoanhsang,
                    "thoigiantuoigannhat" => $thoigiantuoigannhat,
                    "ngaybonphan" => $ngaybonphan,
                );
                $rs = json_decode($this->zone_model->createZone($data));
                if(isset($rs->exitcode))
                {
                    $this->session->set_flashdata('message_success','Tạo khu mới thành công!');
                } else {
                    $this->session->set_flashdata('message_error','Không thêm thành công!');
                }
                redirect('admin/zone','refresh');
          
            }
        }
       
        $getGarden = $this->garden_model->getAllGarden();
        $data['getGarden'] = json_decode($getGarden);
        $data['temp'] ='admin/zone/create';
        $this->load->view('admin/main',$data);
    }

    function edit()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $id = $this->uri->segment(4);
        $getZone = $this->zone_model->getZoneId($id);
        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Tên khu', 'required|min_length[5]');
            $this->form_validation->set_rules('gardenname', 'Tên vườn rau', 'required');
            $this->form_validation->set_rules('location', 'Vị trí', 'required|min_length[5]');
            $this->form_validation->set_rules('nhietdo', 'Nhiệt độ', 'required');
            $this->form_validation->set_rules('type', 'Kiểu', 'required');
            $this->form_validation->set_rules('doamkhongkhi', 'Độ ẩm không khí', 'required');
            $this->form_validation->set_rules('doamdat', 'Độ ẩm đất', 'required');
            $this->form_validation->set_rules('thoigiantuoi', 'Thời gian tưới', 'required');
            $this->form_validation->set_rules('cuongdoanhsang', 'Cường độ ánh sáng', 'required');
           // $this->form_validation->set_rules('thoigiantuoigannhat', 'Thời gian tưới gần nhất', 'required');
          //  $this->form_validation->set_rules('ngaybonphan', 'Ngày bón phân', 'required');
            
            if($this->form_validation->run())
            {
                $name = $this->input->post('name');
                $idGr = $this->input->post('gardenname');
                $location = $this->input->post('location');
                $type = $this->input->post('type');
                $nhietdo = $this->input->post('nhietdo');
                $doamkhongkhi = $this->input->post('doamkhongkhi');
                $doamdat = $this->input->post('doamdat');
                $thoigiantuoi= $this->input->post('thoigiantuoi');
                $cuongdoanhsang = $this->input->post('cuongdoanhsang');
                $thoigiantuoigannhat = $this->input->post('thoigiantuoigannhat');
                $ngaybonphan = $this->input->post('ngaybonphan');
                echo $name;
                $data = array(
                    "zoneId" =>$id,
                    "gardenId"   => $idGr,
                    "name"   => $name, 
                    "location"   => $location,
                    "type"    => $type,
                    "nhietdo"   => $nhietdo,
                    "doamkhongkhi"   => $doamkhongkhi,
                    "doamdat" => $doamdat,
                    "thoigiantuoi" => $thoigiantuoi,
                    "cuongdoas" => $cuongdoanhsang,
                    "thoigiantuoigannhat" => $thoigiantuoigannhat,
                    "ngaybonphan" => $ngaybonphan,
                 
                );
                $rs = json_decode($this->zone_model->updateZone($data));
                echo $id.$name.$location.$type.$cuongdoanhsang;
               
                redirect('admin/zone','refresh');
            }
        }
        $getGarden = $this->garden_model->getAllGarden();
        $data['getZone'] = json_decode($getZone);
        $data['getGarden'] = json_decode($getGarden);
        $data['temp'] ='admin/zone/edit';
        $this->load->view('admin/main',$data);
    }

    function delete()
    {
        if($this->uri->rsegment('2') == "delete")
        {
            $id = $this->uri->rsegment('3'); 
            $data = array(
                "zoneId"   => $id);
            $this->zone_model->deleteZone($data);
        }
        redirect('admin/zone','refresh');
    }

}
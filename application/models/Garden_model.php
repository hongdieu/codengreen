<?php
class Garden_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function getAllGarden()
    {
        $curl = curl_init("fpo.vn:5001/garden/getAll");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        curl_close($curl);
        return  $result;  
    }
    
    public function createGarden($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/garden/create");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    public function getGardenById($id)
    {
        $data = array("gardenId" => $id);
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/garden/getById");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result; 
    }

    public function getZoneOfGarden($id)
    {
        $data = array("gardenId" => $id);
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/zone/getByGarden");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result; 
    }

    public function updateGarden($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/garden/update");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    public function deleteGarden($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/garden/delete");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }
}
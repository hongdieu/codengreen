<?php
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function postLogin($user, $pass)
    {
        $data = array("username" =>$user, "password" => $pass); 

        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/auth/login");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        $result = curl_exec($curl);
        ///$rs = json_encode($result);
        curl_close($curl);
        return  $result;     
    }
    public function getAllUser()
    {
        $curl = curl_init("fpo.vn:5001/user/getAll");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    public function createUser($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/user/create");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    
    public function updateUser($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/user/update");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    public function deleteUser($id)
    {
        $data = array("userId" => $id);
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/user/delete");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }
    public function getUserByUserID($id)
    {
        $data = array("userId" => $id);
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/user/getById");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result; 
    }
    public function getGardenOfUser($id)
    {
        $data = array("userId" => $id);
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/garden/getByUser");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result; 
    }
   
}
?>
<?php
class Zone_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllZone()
    {
        $curl = curl_init("fpo.vn:5001/zone/getAll");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;
    }

    public function createZone($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/zone/create");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    public function deleteZone($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/zone/delete");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result;  
    }

    function updateZone($data)
    {
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/zone/update");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result; 
    }
    function getZoneId($id)
    {
        $data = array("zoneId" => $id);
        $data_string = json_encode($data);
        $curl = curl_init("fpo.vn:5001/zone/getById");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'x-access-token: '.$this->session->userdata("token")
        ));
        $result = curl_exec($curl);
        //$rs = json_encode($result);
        curl_close($curl);
        return  $result; 
    }


}